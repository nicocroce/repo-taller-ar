var clientesObtenidos;

function gestionClientes() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if(this.readyState == 4 && this.status == 200) {
            console.table(JSON.parse(request.responseText).value);
            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    }

    request.open("GET", url, true);
    request.send();
}

function procesarClientes() {
    var JSONpClientes = JSON.parse(clientesObtenidos).value;
    var divTabla = divTablaClientes;
    var tabla = document.createElement("table");
    var tBody = document.createElement("tbody");
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    var nuevaFilaHead = document.createElement("thead");
    nuevaFilaHead.classList.add("thead-dark");
    var tr = document.createElement("tr");

    var nuevoHeaderName = document.createElement("th");
    nuevoHeaderName.innerText = "Nombre Cliente";

    var nuevoHeaderPrice = document.createElement("th");
    nuevoHeaderPrice.innerText = "Compañía";

    var nuevoHeaderStock = document.createElement("th");
    nuevoHeaderStock.innerText = "País";

    tr.appendChild(nuevoHeaderName);
    tr.appendChild(nuevoHeaderPrice);
    tr.appendChild(nuevoHeaderStock);
    nuevaFilaHead.appendChild(tr);

    tabla.appendChild(nuevaFilaHead);

    JSONpClientes.map(value => {
        var nuevaFila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = value.ContactName;

        var columnaCompany = document.createElement("td");
        columnaCompany.innerText = value.CompanyName;

        var columnaCountry = document.createElement("td");
        var imgBandera = document.createElement("img");
        var countryName = value.Country == "UK" ? "United-Kingdom" : value.Country;
        imgBandera.src = rutaBandera + countryName + ".png";
        imgBandera.classList.add("flag");
        columnaCountry.appendChild(imgBandera);

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCompany);
        nuevaFila.appendChild(columnaCountry);

        tBody.appendChild(nuevaFila);
    });

    tabla.appendChild(tBody);
    divTabla.appendChild(tabla);

}