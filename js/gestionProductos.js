var productosObtenidos;

function gestionProductos() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if(this.readyState == 4 && this.status == 200) {
            console.table(JSON.parse(request.responseText).value);
            productosObtenidos = request.responseText;
            procesarProductos();
        }
    }

    request.open("GET", url, true);
    request.send();
}

function procesarProductos() {
    var JSONpProductos = JSON.parse(productosObtenidos).value;
    var divTabla = divTablaProductos;
    var tabla = document.createElement("table");
    var tBody = document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    tabla.classList.add("table-dark");

    var nuevaFilaHead = document.createElement("thead");
    nuevaFilaHead.classList.add("thead-light");
    var tr = document.createElement("tr");

    var nuevoHeaderName = document.createElement("th");
    nuevoHeaderName.innerText = "Nombre Producto";

    var nuevoHeaderPrice = document.createElement("th");
    nuevoHeaderPrice.innerText = "Precio";

    var nuevoHeaderStock = document.createElement("th");
    nuevoHeaderStock.innerText = "Stock";

    tr.appendChild(nuevoHeaderName);
    tr.appendChild(nuevoHeaderPrice);
    tr.appendChild(nuevoHeaderStock);
    nuevaFilaHead.appendChild(tr);

    tabla.appendChild(nuevaFilaHead);

    JSONpProductos.map(value => {
        var nuevaFila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = value.ProductName;

        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText = value.UnitPrice;

        var columnaStock = document.createElement("td");
        columnaStock.innerText = value.UnitsInStock;

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);

        tBody.appendChild(nuevaFila);
    });

    tabla.appendChild(tBody);
    divTabla.appendChild(tabla);

}